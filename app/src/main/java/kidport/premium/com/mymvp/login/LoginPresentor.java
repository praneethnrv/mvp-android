package kidport.premium.com.mymvp.login;

import kidport.premium.com.mymvp.model.login.sync.SynchronousLoginInteractor;

/**
 * Created by Praneeth on 3/29/2017.
 */

public class LoginPresentor implements ILoginPresentor{

    private ILoginView iLoginView;
    private SynchronousLoginInteractor interactor;

    public LoginPresentor(ILoginView loginView){
        iLoginView = loginView;
        this.interactor = new SynchronousLoginInteractor();
    }

    public void attemptLogin(String loginEmail, String loginPassword) {
        boolean isValid = interactor.validateCredentials(loginEmail,loginPassword);
        if (isValid) iLoginView.navigateToListActivity();else iLoginView.loginFailed();
    }
}
