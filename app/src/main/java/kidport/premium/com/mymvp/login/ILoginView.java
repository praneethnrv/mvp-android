package kidport.premium.com.mymvp.login;

/**
 * Created by Praneeth on 3/29/2017.
 */

public interface ILoginView {

    void navigateToListActivity();
    void loginFailed();

}
