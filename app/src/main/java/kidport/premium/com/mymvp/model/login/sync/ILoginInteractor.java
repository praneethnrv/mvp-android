package kidport.premium.com.mymvp.model.login.sync;

/**
 * Created by Praneeth on 3/29/2017.
 */

interface ILoginInteractor {
    boolean validateCredentials(String username,String password);
}
