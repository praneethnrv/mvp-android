package kidport.premium.com.mymvp.ui;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import kidport.premium.com.mymvp.BuildConfig;
import kidport.premium.com.mymvp.R;
import kidport.premium.com.mymvp.login.ILoginView;
import kidport.premium.com.mymvp.login.LoginPresentor;

public class LoginActivity extends AppCompatActivity implements ILoginView {

    @InjectView(R.id.edittext_login_username)
    EditText edittextLoginUsername;

    @InjectView(R.id.edittext_login_password)
    EditText edittextLoginPassword;

    LoginPresentor presentor;
    Dialog processDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.inject(this);

        if(BuildConfig.DEBUG){
            edittextLoginUsername.setText("dummyemail.curo+basic1@gmail.com");
            edittextLoginPassword.setText("test1234");
        }

        //Give the presentor a reference view here
        presentor = new LoginPresentor(this);
    }

    @OnClick(R.id.button_login_click)
    public void loginTapped(View view){
        processDialog = new Dialog(this);
        processDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        processDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        processDialog.setContentView(R.layout.dialog_layout);
        String loginEmail = edittextLoginUsername.getText().toString();
        String loginPassword = edittextLoginPassword.getText().toString();
        //Pass the event straight to presentor
        presentor.attemptLogin(loginEmail,loginPassword);
    }

    @Override
    public void navigateToListActivity() {
        Toast.makeText(getApplicationContext(),"Login Credential valied",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void loginFailed() {
        Toast.makeText(getApplicationContext(),"Invalied Login Credentials",Toast.LENGTH_SHORT).show();
    }
}
